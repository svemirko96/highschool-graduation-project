Simple 2D top down tower defense game.
It loads levels from files and has AI with a basic pathfinding algorithm.

First bigger project I did together with a friend for the highschool graduation project.

![gameplay preview](https://gitlab.com/svemirko96/highschool-graduation-project/-/raw/master/preview.PNG)
