import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class LevelLoader {
	
	private LevelManager levelManager;
	
	public LevelLoader(LevelManager levelManager){
		this.levelManager = levelManager;
	}
	
	public void loadLevel(int lvl){
		try{
			Scanner sc = new Scanner(new File("level/level" + lvl + ".mapa"));
		
			levelManager.mobs = new Mob[sc.nextInt()];
			
			for(int yC = 0; yC < LevelManager.MAP_HEIGHT; yC++)
				for(int xC = 0; xC < LevelManager.MAP_WIDTH; xC++)
					levelManager.blockAt(yC, xC).init(xC*Block.SIZE, yC*Block.SIZE, sc.nextInt());
				
			
			levelManager.mobExitPoint.x = sc.nextInt();
			levelManager.mobExitPoint.y = sc.nextInt();
			
			sc.close();
		}
		catch(IOException e){
			System.err.println("Failed loading level file");
		}
	}
	
}
