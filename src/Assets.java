import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.annotation.Resource;
import javax.imageio.ImageIO;


public class Assets {

	private static final int groundTilesetSize = 10;
	
	public static BufferedImage shopBackground;
	public static BufferedImage menuBackground;
	
	//velika slika
	private static BufferedImage groundTileset;
	private static BufferedImage mobAnimationFrames;
	
	//iseckani delovi velike
	public static BufferedImage[] groundTiles = new BufferedImage[groundTilesetSize*groundTilesetSize];
	public static BufferedImage[] mobFrames = new BufferedImage[4];
	
	public static BufferedImage tower;
	public static BufferedImage towerIcon;
	public static BufferedImage dollarIcon;
	
	public static void loadImages(){
		try{
			
			shopBackground = ImageIO.read(new File("rsc/shop.png"));
			menuBackground = ImageIO.read(new File("rsc/menu.png"));
			
			//ucitava se velika iz rsc foldera
			groundTileset = ImageIO.read(new File("rsc/tileset64p_10x10.png"));
			mobAnimationFrames = ImageIO.read(new File("rsc/mob_animation.png"));
			
			//secka se velika u 100 malih slika
			for(int y = 0; y < groundTilesetSize; y++)
				for(int x = 0; x < groundTilesetSize; x++){
					groundTiles[y*groundTilesetSize + x] = groundTileset.getSubimage(x*Block.SIZE, y*Block.SIZE, Block.SIZE, Block.SIZE);
				}
			for(int i = 0; i < mobFrames.length; i++)
				mobFrames[i] = mobAnimationFrames.getSubimage(i * Block.SIZE, 0, Block.SIZE, Block.SIZE);
			
			tower = ImageIO.read(new File("rsc/tower.png"));
			towerIcon = ImageIO.read(new File("rsc/towerIcon.png"));
			dollarIcon = ImageIO.read(new File("rsc/dollarIcon.png"));
		}
		catch(IOException e){
			System.err.println("Failed loading assets.");
			}
	}
	
}
