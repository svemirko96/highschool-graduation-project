import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;

@SuppressWarnings("serial")
public class Mob extends Rectangle{

	public int hp, maxhp, xC, yC, speed, walked;
	private int hpBarWidth, hpBarHeight, hpBarGap;
	private Point direction;
	public boolean inGame = false;
	public boolean isAlive = true;
	
	private LevelManager levelManager;
	public Circle shape;
	
	public Mob(int xC, int yC, LevelManager levelManager){
		this.levelManager = levelManager;
		setBounds(xC * Block.SIZE, yC * Block.SIZE, Block.SIZE, Block.SIZE);
		this.xC = xC;
		this.yC = yC;
		maxhp = Block.SIZE;
		hp = maxhp;
		speed = 2;
		direction = new Point(1, 0);
		walked = 0;
		hpBarWidth = Block.SIZE;
		hpBarHeight = 8;
		hpBarGap = 10;
		shape = new Circle(x, y, Block.SIZE / 2);
	}
	
	public void spawnMob(){
		inGame = true;
	}
	
	public void damaged(int dmg){
		hp -= dmg;
	}
	
	public void update(){
		
		if(hp <= 0){
			hp = 0;
			inGame = false;
			isAlive = false;
			LevelManager.player_money += 5;
		}
		
		if(inGame){
			if(walked == Block.SIZE){
				//dodje do ovde
				xC += direction.x;
				yC += direction.y;
				
				if(xC == levelManager.mobExitPoint.x && yC == levelManager.mobExitPoint.y){
					LevelManager.player_hp -= 10;
					inGame = false;
					isAlive = false;
					return;
				}
				
				if(levelManager.blockAt(yC, xC + 1).getID() == Block.roadID && direction.x != -1){
					direction.x = 1;
					direction.y = 0;
				}
				else if(levelManager.blockAt(yC, xC - 1).getID() == Block.roadID && direction.x != 1){
					direction.x = -1;
					direction.y = 0;
				}
				else if(levelManager.blockAt(yC + 1, xC).getID() == Block.roadID && direction.y != -1){
					direction.x = 0;
					direction.y = 1;
				}
				else if(levelManager.blockAt(yC - 1, xC).getID() == Block.roadID && direction.y != 1){
					direction.x = 0;
					direction.y = -1;
				}
				walked = 0;
			}
			
			x += speed * direction.x;
			y += speed * direction.y;
			
			walked += speed;
		}
	}
	
	int swapFrame =0, swapTime = 10, currentFrame = 0;
	public void draw(Graphics g){
		
		Graphics2D g2d = (Graphics2D)g;
		
		if(inGame){
			if(swapFrame == swapTime){
				currentFrame ++;
				currentFrame %= 4;
				swapFrame = 0;
			}
			else
				swapFrame++;
			
			g2d.drawImage(Assets.mobFrames[currentFrame], x, y, null);
			g2d.setColor(Color.RED);
			g2d.fillRect(x, y - hpBarGap, hpBarWidth, hpBarHeight);
			g2d.setColor(new Color(50, 255, 100));
			g2d.fillRect(x, y - hpBarGap, hp, hpBarHeight);
			g2d.setColor(new Color(100, 20, 20));
			g2d.drawRect(x, y - hpBarGap, hpBarWidth, hpBarHeight);
		}
	}
}
