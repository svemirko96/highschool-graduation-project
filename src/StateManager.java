
public class StateManager {

	public static boolean preGame = true;
	public static boolean gameState = false;
	public static boolean pauseState = false;
	public static boolean gameOverState = false;
	
	public static void startGame(){
		System.out.println("starting game");
		preGame = false;
		gameState = true;
		pauseState = false;
		gameOverState = false;
	}
	
	public static void pauseGame(){
		preGame = false;
		gameState = false;
		pauseState = true;
		gameOverState = false;
	}
	
	public static void resumeGame(){
		preGame = false;
		gameState = true;
		pauseState = false;
		gameOverState = false;
	}
	
	public static void gameOver(){
		preGame = false;
		gameState = false;
		pauseState = false;
		gameOverState = true;
	}
	
}
