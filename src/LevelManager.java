import java.awt.Graphics;
import java.awt.Point;


public class LevelManager {
	
	public static final int MAP_WIDTH = 16;
	public static final int MAP_HEIGHT = 10;
	public static Block[][] map;
	public Mob[] mobs;
	public Point mobStartPoint;
	public Point mobExitPoint;
	
	public static int player_hp = 100;
	public static int player_money = 30;
	
	private LevelLoader levelLoader; 
	private int stoppedAt = 0;
	
	public LevelManager(){
		levelLoader = new LevelLoader(this);
		map = new Block[MAP_HEIGHT][MAP_WIDTH];
	}
	
	//------------------------------------------------------------------------------------------------
	// ONCE BEFORE UPDATE
	public void initLevel(int currentLevel){		
		
		player_hp = 100;
		stoppedAt = 0;
		
		for(int i = 0; i < MAP_WIDTH * MAP_HEIGHT; i++){
			map[i / MAP_WIDTH][i % MAP_WIDTH] = new Block();
		}
		
		mobExitPoint = new Point();
		levelLoader.loadLevel(currentLevel); // inicijalizuje sve blokove na mapi 
		
		for(int y = 0; y < MAP_HEIGHT; y++)
			if(map[y][0].getID() == Block.roadID){  // pronalazi pocetnu tacku mobovima
				mobStartPoint = new Point(0, y);
				break;
			}
		
		for(int i = 0; i < mobs.length; i++){
			mobs[i] = new Mob(mobStartPoint.x, mobStartPoint.y, this);   // inicijalizuje mobove
		}
	}
	//------------------------------------------------------------------------------------------------
	// UPDATE
	static int spawnFrame = 0, spawnTime = 120;
	public void updateLevel(){
		if(player_hp > 0){
			if(spawnFrame >= spawnTime)
				for(int i = stoppedAt; i < mobs.length; i++){
					if(!mobs[i].inGame){
						mobs[i].spawnMob();						// na svake dve sekunde ubacuje po jednog moba iz niza u igru
						stoppedAt++;
						spawnFrame = 0;
						break;
					}
					
				}
			else
				spawnFrame++;
			
			for(int y = 0; y < MAP_HEIGHT; y++)
				for(int x = 0; x < MAP_WIDTH; x++){
					if(map[y][x].getTower() != null)		// apdejtuje tornjeve
						map[y][x].getTower().update(mobs);
				}
			
			for(Mob mob : mobs)
				if(mob.inGame)								// apdejtuje mobove
					mob.update();
				
		}
		else{
			StateManager.gameOver();
		}
	}
	//------------------------------------------------------------------------------------------------------
	// RENDER
	public void drawLevel(Graphics g){
		
		for(int y = 0; y < MAP_HEIGHT; y++)			// crta blokove
			for(int x = 0; x < MAP_WIDTH; x++){
				map[y][x].drawBlock(g);
			}
		
		for(int i = 0; i < mobs.length; i++)		// crta mobove
			if(mobs[i].inGame)
				mobs[i].draw(g);
		
		for(int y = 0; y < MAP_HEIGHT; y++)
			for(int x = 0; x < MAP_WIDTH; x++){		// crta tornjeve
				if(map[y][x].getTower() != null)
					map[y][x].getTower().draw(g);
			}
		
	}
	//-------------------------------------------------------------------------------------------------------
	// OSTALO
	public void spawnTower(int xC, int yC){
		
		if(!map[yC][xC].hasTower() && map[yC][xC].getID() == Block.grassID && player_money >= 10){
			map[yC][xC].spawnTower();
			player_money -= 10;
		}
		//info
	}
	
	public boolean checkIfLevelDone(){
		for(Mob mob : mobs)
			if(mob.isAlive)
				return false;
		return true;
	}
	
	public void removeTower(int xC, int yC){
		map[yC][xC].removeTower();
		player_money += 5;
	}
	
	public Block blockAt(int y, int x){
		Block t = map[y][x];
		return t;
	}
	
}
