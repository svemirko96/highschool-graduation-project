import javax.swing.JFrame;


@SuppressWarnings("serial")
public class Main extends JFrame{

	public static final String TITLE = "TD za Maturski";
	public Screen screen;
	
	public static boolean debugMode = true;
	
	public Main(){
		setTitle(TITLE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		screen = new Screen();
		add(screen);
		pack();
		setLocationRelativeTo(null);
	}
	
	public static void main(String[] args){
		new Main().setVisible(true);
	}
	
}
