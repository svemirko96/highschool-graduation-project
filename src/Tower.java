//import java.awt.Color;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;


@SuppressWarnings("serial")
public class Tower extends Rectangle implements Comparable<Tower>{

	//@SuppressWarnings("unused")
	private int x, y, xC,  yC;
	public int damage = 10;
	public Circle crange;
	public Rectangle rrange;
	public boolean shooting = false;
	public boolean hasTarget = false;
	public boolean attackVisible = true;
	public int targetIndex;
	private Point targetPosition = new Point();
	public boolean inGame;
	//private boolean rangeVisible = false;
	
	//private boolean cMode = false, rMode = true;
	
	public Tower(Block block){
		x = block.x;
		y = block.y;
		xC = x / Block.SIZE;
		yC = y / Block.SIZE;
		crange = new Circle(block.x - Block.SIZE * 2, block.y - Block.SIZE * 2, Block.SIZE * 5 / 2);
		rrange = new Rectangle(block.x - Block.SIZE * 2, block.y - Block.SIZE * 2, Block.SIZE * 5, Block.SIZE * 5);
		targetIndex = -1;
	}
	
	public int getXC(){
		return xC;
	}
	
	public int getYC(){
		return yC;
	}
	
	@Override
	public int compareTo(Tower t) {
		return yC - t.getYC();
	}
	// ~~
	public void showInfo(){
		System.out.println("Toranj ovog bloka se nalazi na " + x + " : " + y);
	}
	
	public Rectangle getRange(){
		return rrange;
	}
	
	private void drawAttack(){
		attackVisible = true;
	}
	
	//-------------------------------------------------------------------------
	//---------- UPDATE -------------------------------------------------------
	private int attackFrame = 60, attackTime = 60;
	public void update(Mob[] mobs){
		if(inGame){
			for(int i = 0; i < mobs.length; i++){
				if(mobs[i].inGame){
					if(targetIndex == -1 && rrange.intersects(mobs[i])) // ako trenutno nema metu a ima domet
						targetIndex = i;
					if(targetIndex != -1 && (!rrange.intersects(mobs[targetIndex]) || !mobs[targetIndex].inGame)){	// gubi metu ako je meta je unistena ili van dometa
						targetIndex = -1;
					}
					if(targetIndex != -1 && mobs[targetIndex].inGame){
						targetPosition.x = mobs[targetIndex].x;
						targetPosition.y = mobs[targetIndex].y;
						shooting = true;
					}
				}
			}
			
			if(shooting && targetIndex != -1){
				if(attackFrame >= attackTime && mobs[targetIndex].hp > 0){
					mobs[targetIndex].damaged(damage);
					drawAttack();
					attackFrame = 0;
					shooting = false;
				}
				else
					attackFrame++;
			}
		}
	}
	//----------------------------------------------------------------------------
	//------------ RENDER --------------------------------------------------------
	private int laserFrame = 0, laserStay = 20;
	public void draw(Graphics g){

		Graphics2D g2d = (Graphics2D)g;
		
		if(inGame){
			g2d.drawImage(Assets.tower, x, y - Block.SIZE, null);
			
			if(targetIndex != -1 && attackVisible){
				g.setColor(Color.CYAN);
				g.drawLine(x + Block.SIZE/2 - 5, y - Block.SIZE + 4, targetPosition.x + Block.SIZE / 2, targetPosition.y + Block.SIZE / 2 - 1);
				g.drawLine(x + Block.SIZE/2 - 4, y - Block.SIZE + 5, targetPosition.x + Block.SIZE / 2 + 1, targetPosition.y + Block.SIZE / 2);
				g.drawLine(x + Block.SIZE/2 - 5, y - Block.SIZE + 6, targetPosition.x + Block.SIZE / 2, targetPosition.y + Block.SIZE / 2 + 1);
				g.drawLine(x + Block.SIZE/2 - 6, y - Block.SIZE + 5, targetPosition.x + Block.SIZE / 2 - 1, targetPosition.y + Block.SIZE / 2);
				
				g.setColor(Color.WHITE);
				g.drawLine(x + Block.SIZE/2 - 5, y - Block.SIZE + 5, targetPosition.x + Block.SIZE / 2, targetPosition.y + Block.SIZE / 2);
				if(laserFrame <= laserStay)
					laserFrame++;
				else{
					attackVisible = false;
					laserFrame = 0;
				}
			}
		}
		//ovde treba da stoji kod za pokazivanje dometa a ne u showRange
	}
	
}
