import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class GameScreen extends JPanel{

	public static final int WIDTH = 1024;
	public static final int HEIGHT = 640;
	public static final Dimension size = new Dimension(WIDTH, HEIGHT);
	public static final Rectangle gameScreenArea = new Rectangle(StorePanel.WIDTH, 0, WIDTH, HEIGHT);
	
	private Screen screen;
	private LevelManager levelManager;
	
	public GameScreen(Screen screen, LevelManager levelManager){
		this.levelManager = levelManager;
		this.screen = screen;
		setMinimumSize(size);
		setMaximumSize(size);
		setPreferredSize(size);
		setBackground(Color.CYAN);
	}
	
	public void clicked(int mouseButton, Point p){
		
		int xC = p.x / Block.SIZE;
		int yC = p.y / Block.SIZE;
		
		if(mouseButton == 1)
			if(screen.holdingTower)
				levelManager.spawnTower(xC, yC);
			else if(screen.sellMode)
				levelManager.removeTower(xC, yC);
		
		if(mouseButton == 3){
			screen.holdingTower = false;
			screen.sellMode = false;
		}
		
		
		if(mouseButton == 2 && levelManager.blockAt(yC, xC).getTower() != null)
			levelManager.blockAt(yC, xC).getTower().showInfo();
		
		
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		
		if(StateManager.gameState || StateManager.pauseState)
			levelManager.drawLevel(g);
	}
	
}
